//
//  UIImage+MMColor.h
//  mmdays
//
//  Created by lilo on 16/2/23.
//  Copyright © 2016年 lilo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (MMColor)

+ (id)imageWithColor:(UIColor*)color;

@end
