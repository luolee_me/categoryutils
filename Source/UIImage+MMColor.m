//
//  UIImage+MMColor.m
//  mmdays
//
//  Created by lilo on 16/2/23.
//  Copyright © 2016年 lilo. All rights reserved.
//

#import "UIImage+MMColor.h"

@implementation UIImage (MMColor)

+ (id)imageWithColor:(UIColor*)color
{
    CGRect rect = CGRectMake(0, 0, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
